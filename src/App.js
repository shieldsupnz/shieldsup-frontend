import React from 'react';
import "picnic/picnic.min.css";
import './App.css';
import 'moment-timezone';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";


import Requests from "./app/components/requests/requests"
import RequestDetail from "./app/components/requestDetails/requestDetail"
import Login from "./app/components/login/login"
import Logout from "./app/components/login/logout"
import Stock from "./app/components/stock/stock"

function App() {

  return (
    <div className="App">
 <Router>
      <div>
        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route exact path="/request/:id" component={RequestDetail} />
          <Route path="/stock" component={Stock}/>
          <Route path="/" component={Requests}/>

         
        </Switch>
      </div>
    </Router>
    </div>
  );
}

export default App;
