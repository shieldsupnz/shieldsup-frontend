import React ,{useEffect}from 'react'
import {Redirect} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import {
  logout,
  selectUser,
} from './authSlice';

export default function Logout(){
  const user = useSelector(selectUser);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch( dispatch(logout));
  }, [dispatch]);
 

  if (!user || !user.isAuthenticated) {
    return <Redirect to="/login" />
  }


    return (null)
}





