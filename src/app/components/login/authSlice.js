import { createSlice } from '@reduxjs/toolkit';
import API from "../../utils/api";

const logoutLocal = (state,action) =>{
  localStorage.removeItem("token");
  state.token =null;
  state.user = null;
  state.errors = action.payload;
}

export const slice = createSlice({
  name: 'auth',
  initialState: {
    token: localStorage.getItem("token"),
    user: {user_id:0, manufacturer_id: 0},
    errors: [],
  },
  reducers: {

    loginSuccessful: (state, action) =>{
      localStorage.setItem("token", action.payload.token);
      state.token = action.payload.token;
      state.user = action.payload;
      state.user.isAuthenticated =true;
      state.errors = null;
    },

    authenticationError:(state,action) =>logoutLocal(state,action),
    loginFailed:(state,action) =>logoutLocal(state,action),
    logoutSuccessful:(state,action) =>logoutLocal(state,action),

  },
});



export const { loginSuccessful, authenticationError, loginFailed, logoutSuccessful} = slice.actions;



export const login = (username, password) => {
  return (dispatch, getState) => {
    let headers = {"Content-Type": "application/json"};

    return API.post('/api-token-auth/', {username:username, password:password}, headers)
      .then(res => {
        if (res.status === 200) {
          console.log(res.data)
          dispatch(loginSuccessful(res.data ));
          return res.data;
        } else if (res.status === 403 || res.status === 401) {
          dispatch(authenticationError(res.data));
          throw res.data;
        } else {
          dispatch(loginFailed(res.data));
          throw res.data;
        }
      })
  }
}

export const logout = () => {
    return (dispatch, getState) => {
      let {token} = getState().auth;
      let tokenstring = '';
      if (token) {
        tokenstring = `Token ${token}`;
      }
      API.defaults.headers.common['Authorization'] = tokenstring;

      dispatch(logoutSuccessful({}));

    return API.get('/api-token-logout/')
      .then(res => {
        if (res.status === 200) {
          console.log(res.data)
          return res.data;
        }
      })
  }
}

export const selectUser = state => state.auth.user;
export const selectErrors = state => state.auth.errors;

export default slice.reducer;
