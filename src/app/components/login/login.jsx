import React, { useState, Fragment } from 'react'
import {Header} from '../header/headerComponent'
import {Link, Redirect} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import {
  login,
  selectUser,
  selectErrors,
} from './authSlice';

export default function Login(){
  const user = useSelector(selectUser);
  const errors = useSelector(selectErrors);
  const dispatch = useDispatch();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  if (user && user.isAuthenticated) {
    return <Redirect to="/" />
  }

  const onSubmit = e => {
    e.preventDefault();
    dispatch(login(username, password));
  }

    return (
      <Fragment>
        <Header />
        <div className="container">
          <h2>Login</h2>
          {errors.length > 0 && (
            <ul>
              {errors.map(error => (
                <li key={error.field}>{error.message}</li>
              ))}
            </ul>
          )}
          <form onSubmit={onSubmit}>
            <fieldset>
              <p>
                <label htmlFor="username">Username</label>
                <input
                  type="text" id="username"
                  onChange={e => setUsername(e.target.value)} />
              </p>
              <p>
                <label htmlFor="password">Password</label>
                <input
                  type="password" id="password"
                  onChange={e => setPassword(e.target.value)} />
              </p>
              <p>
                <button type="submit">Login</button>
              </p>

              <p>
                Don't have an account? <Link to="#">Register</Link>
              </p>
            </fieldset>
          </form>
        </div>
      </Fragment>
    )
}





