import React, { useState, useRef, useSelector }from 'react'
import { Link, Redirect } from 'react-router-dom'
import  './headerComponent.css'
import logo from './shieldsuplogo.png'

import Burger from '../burger/burger';
import Menu from '../menu/menu';
import { useOnClickOutside } from '../../hooks/useOnClickOutside';


export const Header = props => {
  const [open, setOpen] = useState(false);
  const node = useRef(); 
  useOnClickOutside(node, () => setOpen(false));

 


  return(<div className="topNav">
    <div className="navInner">
      <img src={logo} alt="logo" className="logo"/>     
      <div className="navbar">
        <Link className="link" to="/request">
            Requests
        </Link>
        <Link className="link" to="/stock">
            Stock & Supplies
        </Link>
        <Link className="link" to="/logout">
            Log Out
        </Link>
      </div>
      <div className="hamburgerMenu" ref={node}>
          <Burger open={open} setOpen={setOpen}/>
          <Menu open={open} setOpen={setOpen}/>
      </div>
    </div>
  </div>)
}