import React, {useEffect, Fragment, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import {Header} from '../header/headerComponent'
import Moment from 'react-moment';
import {Link, Redirect} from "react-router-dom";

import './requestDetail.css';
import {
  selectUser,
} from '../login/authSlice';

import {
  fetchOrder,
  setDelivered,
  selectOrder,
  selectIsLoading,
} from '../requests/ordersSlice';

export default function RequestDetail(props) {
  const user = useSelector(selectUser);
  const order = useSelector(selectOrder);
  const isLoading = useSelector(selectIsLoading);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchOrder(props.match.params.id));
  }, [dispatch, props.match.params.id]);
  const [amount, setAmount] = useState('');

  console.log("user", user);
  if (!user || !user.isAuthenticated) {
    return <Redirect to="/login" />
  }
  const handleChange =(e)=>{
    setAmount(e.target.value);
  }

  const markAsDelivered =(e)=>{
    dispatch(setDelivered(props.match.params.id, amount));
  }

  return (
    <Fragment>
      <Header />
      <div className="container">
        <h2>Request Details</h2>
        {!isLoading && order &&<div>
            <dl className="requestDetails">
            <dt >Priority</dt><dd className={order.priority}>{order.priority}</dd>
            <dt>Contact Name</dt><dd>{order.customer.name}</dd>
            <dt>Organisation</dt><dd>{order.customer.organization}</dd>
            <dt className='double'>Location</dt><dd className='double'><a href={`https://maps.google.com/?q=${order.delivery_address}`}>{order.delivery_address}</a></dd>
            <dt>Amount Needed</dt><dd>{order.quantity_needed}</dd>
            <dt>Extra Wanted</dt><dd>{order.quantity_wanted}</dd>
            <dt>Phone</dt><dd><a  href={`tel:${order.customer.phone}`}>{order.customer.phone}</a></dd>
            <dt>Email</dt><dd><a href={`mailto:${order.customer.email}`}>{order.customer.email}</a></dd>
            <dt>Request Date</dt><dd><Moment format="DD/MM/YYYY" >{order.customer.created_at}</Moment></dd>
            <dt>Notes</dt><dd>{order.comments}</dd>
          </dl>
          {!isLoading && order && order.state !== 'shipped' &&<div>
            <p className="amount">
              <label>Amount Delivered:</label><input type="number" value={amount} onChange={handleChange}/>
            </p>
            <button className='success big-button' disabled={!amount || amount === 0} onClick={markAsDelivered}>Mark as Delivered</button>
          </div>}
          {!isLoading && order && order.state === 'shipped' &&<div>
            <p className="amount">
              <p>Request completed: <strong>{order.quantity_delivered}</strong> delivered</p>
            </p>
          </div>}
        </div>}
      </div>
    </Fragment>
  );
  
}
