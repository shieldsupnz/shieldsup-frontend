import React from 'react';
import { Link } from 'react-router-dom'
import  './menu.css'

const Menu = ({open, setOpen}) => {

  const topLine = (open ? {transform:'translateX(0)'} : {transform:'translateX(100%)'});

  return (<div className='menu' style={topLine} onClick={() => setOpen(!open)}>
          <Link className="link menu-item" to="/request">
              Requests
          </Link>
          <Link className="link menu-item" to="/stock">
              Stock & Supplies
          </Link>
          <Link className="link menu-item" to="/logout">
              Log Out
          </Link>
        </div>
      );

  }
  export default Menu;