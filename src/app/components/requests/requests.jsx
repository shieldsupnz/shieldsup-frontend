import React, {useState, useEffect, Fragment } from 'react'
import {Link, Redirect} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';
import {Header} from '../header/headerComponent'
import { FaCheck } from 'react-icons/fa';
import {
  selectUser,
} from '../login/authSlice';

import {
  fetchOrders,
  selectOrders,
  selectIsLoading,
} from './ordersSlice';
import './requests.css';

export default function Requests(props) {
  const user = useSelector(selectUser);
  const manufacturer_id = user && user.manufacturer_id? user.manufacturer_id: 0;
  const orders = useSelector(selectOrders);
  const isLoading = useSelector(selectIsLoading);
  const dispatch = useDispatch();
  let rankedOrders = [];

  useEffect(() => {
    dispatch(fetchOrders(manufacturer_id));
  }, [dispatch, manufacturer_id]);

  console.log("user", user);
  if (!user || !user.isAuthenticated) {
    return <Redirect to="/login" />
  }

  if(orders){
    console.log(orders);
    rankedOrders = orders.map(o=>({...o}));
    rankedOrders.forEach( o =>(o.rank = o.state === 'shipped' ? 4 : (o.priority === 'high' ? 1 : (o.priority === 'normal' ? 2 : 3))))
    rankedOrders.sort((a, b) => a.rank - b.rank);
    console.log(rankedOrders);
  }

 
 const handleClick = (id) =>{
   props.history.push('/request/'+ id)
 }
    
  return (
    <Fragment>
      <Header />
      <div className="container">
        <h2>Requests assigned to you</h2> 
        {/* <p>Your Hub Lead is <a href="#">Campbell Wright</a></p> */}
        <table className="requestsTable">
          <tbody>
          <tr><th></th><th>Name</th><th className="quantity">No.</th></tr>
  {!isLoading && rankedOrders && rankedOrders.map((r,i)=> <tr key={r.id} className={`${r.priority} ${r.state}`} onClick={()=> handleClick(r.id)}><td className={`${r.priority} ${r.state}`}>{r.state !== "shipped" ?<span>•</span>:<FaCheck/>}</td><td><strong>{r.customer.organization}</strong><br/>{r.customer.name}</td><td className="quantity">{r.quantity_needed}</td></tr>)}
          {!isLoading && orders && orders.length === 0 && <tr><td colSpan={3} className="noRequests"><strong>You have no assigned requests, contact your Hub Lead</strong></td></tr>}

          </tbody>
        </table>
      </div>
    </Fragment>
  );
  
}



