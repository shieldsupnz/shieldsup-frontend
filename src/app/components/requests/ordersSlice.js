import { createSlice } from '@reduxjs/toolkit';
import API from "../../utils/api";


export const slice = createSlice({
  name: 'orders',
  initialState: {
    orders: null,
    order: null,
    isLoading: false,
    errors: null,
  },
  reducers: {
    ordersPending:(state) =>{
      state.orders = null;
      state.isLoading =true;
      state.errors = null;
    },
    ordersSuccess: (state, action) =>{
      state.orders = action.payload;
      state.isLoading =false;
      state.errors = null;
    },
    ordersError:(state,action) =>{
      state.orders = null;
      state.isLoading =false;
      state.errors = action.payload;
    },
    orderPending:(state) =>{
      state.order = null;
      state.isLoading =true;
      state.errors = null;
    },
    orderSuccess: (state, action) =>{
      state.order = action.payload;
      state.isLoading =false;
      state.errors = null;
    },
    orderError:(state,action) =>{
      state.order= null;
      state.isLoading =false;
      state.errors = action.payload;
    },
    setDeliveredPending:(state) =>{
      state.order = null;
      state.isLoading =true;
      state.errors = null;
    },
    setDeliveredSuccess: (state, action) =>{
      state.order = action.payload;
      state.isLoading =false;
      state.errors = null;
    },
    setDeliveredError:(state,action) =>{
      state.order= null;
      state.isLoading =false;
      state.errors = action.payload;
    },
  },
});



export const { ordersPending, ordersSuccess, ordersError, orderPending, orderSuccess, orderError, setDeliveredPending, setDeliveredSuccess, setDeliveredError} = slice.actions;



export const fetchOrders = (user_id) => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(ordersPending());
    return API.get('/assigned-orders/'+user_id+'/')
      .then(res => {
        if (res.status === 200) {
          dispatch(ordersSuccess(res.data ));
          return res.data;
        } else {
          dispatch(ordersError(res.data));
          throw res.data;
        }
      })
  }
}

export const fetchOrder = (order_id) => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(orderPending());
    return API.get('/orders/'+order_id+'/')
      .then(res => {
        if (res.status === 200) {
          dispatch(orderSuccess(res.data ));
          return res.data;
        } else {
          dispatch(orderError(res.data));
          throw res.data;
        }
      })
  }
}

export const setDelivered = (order_id, amount) => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(setDeliveredPending());
    return API.put('/order-delivered/'+order_id+'/',{amount: amount})
      .then(res => {
        if (res.status === 200) {
          dispatch(setDeliveredSuccess(res.data ));
          return res.data;
        } else {
          dispatch(setDeliveredError(res.data));
          throw res.data;
        }
      })
  }
}

export const selectOrders = state => state.orders.orders;
export const selectOrder = state => state.orders.order;
export const selectIsLoading = state => state.orders.isLoading;
export const selectErrors = state => state.orders.errors;

export default slice.reducer;
