import React from 'react';
import './burger.css';

const Burger = ({open, setOpen}) => {
  return (
    <div className="burger" open={open} onClick={() => setOpen(!open)}>
      <div className="line"/>
      <div className="line"/>
      <div className="line"/>
    </div>
  )
}

export default Burger;