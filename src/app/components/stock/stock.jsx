import React, {useEffect, Fragment, useState, useMount } from 'react'
import { useSelector, useDispatch } from 'react-redux';
import {Header} from '../header/headerComponent'
import {Link, Redirect} from "react-router-dom";
import {
  selectUser,
} from '../login/authSlice';

import './stock.css';

import {
  fetchProducts,
  fetchStocks,
  //fetchStockRequests,
  // setDelivered,
  selectProducts,
  selectStocks,
  upsertStock,
  //selectStockRequests,
  selectIsLoading,
} from './productsSlice';

export default function Stock(props) {
  const user = useSelector(selectUser);
  const manufacturer_id = user && user.manufacturer_id? user.manufacturer_id: 0;

  const products = useSelector(selectProducts);
  const stocks = useSelector(selectStocks);
  // const stockRequests = useSelector(selectStockRequests);
  const isLoading = useSelector(selectIsLoading);
  const dispatch = useDispatch();
  const [enabledStock, setEnabledStock] = useState({});
  const [manufacturedStock, setManufacturedStock] = useState({});

  // let requestedStock = {};
  
  
  useEffect(() => {
    dispatch(fetchProducts(manufacturer_id));
    dispatch(fetchStocks(manufacturer_id));
    //dispatch(fetchStockRequests(user.manufacturer_id));
  }, [dispatch, manufacturer_id]);


  useEffect(() => {
    if(stocks){
      stocks.forEach(s => {
        if(s.product.kind === 'manufactured') setManufacturedStock(m => ({...m,[s.product.id]: s.quantity}));
      })
    }
  }, [stocks]);

  console.log("user", user);
  if (!user || !user.isAuthenticated) {
    return <Redirect to="/login" />
  }
  // if(stockRequests){
  //   stockRequests.forEach(s => {
  //     requestedStock[s.product] = s;
  //   })
  // }
  
  


  const handleStockChange =(e, id)=>{
    setManufacturedStock({...manufacturedStock, [id]:e.target.value});
  }

  const updateStock =(id)=>{
    setEnabledStock({...enabledStock, [id]:true});
  }
  const saveStock =(id)=>{
    setEnabledStock({...enabledStock, [id]:false});
    const stockItem = stocks.find(s => s.product.id === id);
    const product = products.find(p => p.id === id);
    let stockData ={};
    if(stockItem) stockData = {id: stockItem.id, quantity: manufacturedStock[id], product:product}
    if(!stockItem) stockData = {quantity: manufacturedStock[id], product:product}
    console.log(stockData);
    dispatch(upsertStock(user.manufacturer_id, stockData));
  }

  // const requestSupplies =(e)=>{
  //   //dispatch(setDelivered(props.match.params.id, amount));
  // }

  console.log(products);
  console.log(stocks);
  console.log(isLoading);
  console.log(manufacturedStock);

//Todo - this needs to only view stock requests that are active, and not permit duplicates in active states.(waiting & Accepted) Inactive states are shipped, completed, refused
  return (
    <Fragment>
      <Header />
      <div className="container">
        <h2>Stocks & Supplies</h2>
          <h3>Stock on hand you have manufactured</h3>
          {!isLoading && products &&<ul>
            {!isLoading && products && products.filter(p=> p.kind ==='manufactured').map((p,i)=> 
            <li key={p.id}>
              <label>{p.name}:</label>
              <input type="number" value={ manufacturedStock[p.id] !== undefined? manufacturedStock[p.id]: 0 } onChange={(e)=>handleStockChange(e, p.id)} disabled={enabledStock[p.id] === undefined || enabledStock[p.id] === false}/>
              { (enabledStock[p.id] === undefined || enabledStock[p.id] === false) && <button className="ghost" onClick={()=> updateStock(p.id)}>Edit</button>}
              { (enabledStock[p.id] === true) && <button className="primary" onClick={()=> saveStock(p.id)}>Save</button>}
              </li>)}
          </ul>}
          <h3>Supplies you need</h3>
            <p className="soon">Coming soon...</p>
          {/* {!isLoading && products &&<ul>
            {!isLoading && products && products.map((p,i)=> <li key={p.id}><label>{p.name}:</label><button onClick={requestSupplies}>Edit</button></li>)}
          </ul>} */}
          {/* {!isLoading && order && order.state === 'shipped' &&<div>
            <p className="amount">
              <p>Request completed: <strong>{order.quantity_delivered}</strong> delivered</p>
            </p>
          </div>} */}
        </div>
    </Fragment>
  );
  
}
