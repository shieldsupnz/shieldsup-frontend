import { createSlice } from '@reduxjs/toolkit';
import API from "../../utils/api";


export const slice = createSlice({
  name: 'products',
  initialState: {
    products: null,
    stocks: null,
    stockRequests: null,
    isLoading: false,
    errors: null,
  },
  reducers: {
    productsPending:(state) =>{
      state.products = null;
      state.isLoading =true;
      state.errors = null;
    },
    productsSuccess: (state, action) =>{
      state.products = action.payload;
      state.isLoading =false;
      state.errors = null;
    },
    productsError:(state,action) =>{
      state.products = null;
      state.isLoading =false;
      state.errors = action.payload;
    },
    stocksPending:(state) =>{
      state.stocks = null;
      state.isLoading =true;
      state.errors = null;
    },
    stocksSuccess: (state, action) =>{
      state.stocks = action.payload;
      state.isLoading =false;
      state.errors = null;
    },
    stocksError:(state,action) =>{
      state.stocks= null;
      state.isLoading =false;
      state.errors = action.payload;
    },
    stockRequestsPending:(state) =>{
      state.stockRequests = null;
      state.isLoading =true;
      state.errors = null;
    },
    stockRequestsSuccess: (state, action) =>{
      state.stockRequests = action.payload;
      state.isLoading =false;
      state.errors = null;
    },
    stockRequestsError:(state,action) =>{
      state.stockRequests= null;
      state.isLoading =false;
      state.errors = action.payload;
    },
  },
});



export const { productsPending, productsSuccess, productsError, stocksPending, stocksSuccess, stocksError, stockRequestsPending, stockRequestsSuccess, stockRequestsError} = slice.actions;



export const fetchProducts = () => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(productsPending());
    return API.get('/products/')
      .then(res => {
        if (res.status === 200) {
          dispatch(productsSuccess(res.data ));
          return res.data;
        } else {
          dispatch(productsError(res.data));
          throw res.data;
        }
      })
  }
}

export const fetchStocks = (manufacturer_id) => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(stocksPending());
    return API.get('/stocks/'+manufacturer_id+'/')
      .then(res => {
        if (res.status === 200) {
          dispatch(stocksSuccess(res.data ));
          return res.data;
        } else {
          dispatch(stocksError(res.data));
          throw res.data;
        }
      })
  }
}

export const fetchStockRequests = (manufacturer_id) => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(stockRequestsPending());
    return API.get('/stockRequests/'+manufacturer_id+'/')
      .then(res => {
        if (res.status === 200) {
          dispatch(stockRequestsSuccess(res.data ));
          return res.data;
        } else {
          dispatch(stockRequestsError(res.data));
          throw res.data;
        }
      })
  }
}

export const upsertStock = (manufacturer_id, stockData) => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(stocksPending());
    return API.post('/stocks/'+manufacturer_id+'/', stockData)
      .then(res => {
        if (res.status === 200) {
          dispatch(fetchStocks(manufacturer_id));
          return res.data;
        } else {
          dispatch(stocksError(res.data));
          throw res.data;
        }
      })
  }
}

export const upsertStockRequest = (StockRequest) => {
  return (dispatch, getState) => {
    let {token} = getState().auth;
    let tokenstring = '';
    if (token) {
      tokenstring = `Token ${token}`;
    }
    API.defaults.headers.common['Authorization'] = tokenstring;
    dispatch(stockRequestsPending());
    return API.post('/stockRequests/', StockRequest)
      .then(res => {
        if (res.status === 200) {
          dispatch(stockRequestsSuccess(res.data ));
          return res.data;
        } else {
          dispatch(stockRequestsError(res.data));
          throw res.data;
        }
      })
  }
}



export const selectProducts = state => state.products.products;
export const selectStocks = state => state.products.stocks;
export const selectStockRequests = state => state.products.stockRequests;
export const selectIsLoading = state => state.orders.isLoading;
export const selectErrors = state => state.orders.errors;

export default slice.reducer;
