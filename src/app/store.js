import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import authReducer from './components/login/authSlice';
import ordersReducer from './components/requests/ordersSlice';

export default configureStore({
  reducer: {
    counter: counterReducer,
    auth: authReducer,
    orders: ordersReducer,
  },
});
